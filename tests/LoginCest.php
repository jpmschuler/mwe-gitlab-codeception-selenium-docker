<?php
class ExampleComWorksCest
{    
    public function _before(AcceptanceTester $I)
    {
        $I->amOnPage('/');
    }

    public function correctHeaderExists(AcceptanceTester $I)
    {
        $I->see('Example Domain');
    }
}